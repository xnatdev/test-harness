/*
 * web: org.nrg.xapi.rest.data.CatalogApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.test.harness.xapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotAuthenticatedException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.*;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatSubjectdata;
import static org.nrg.xdat.security.helpers.AccessLevel.*;
import org.nrg.xdat.security.helpers.Permissions;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Api("XNAT Permissions API")
@SuppressWarnings("RedundantThrows")
@XapiRestController
@RequestMapping(value = "/test/permissions")
@Slf4j
public class TestPermissionsApi extends AbstractXapiRestController {
    @Autowired
    public TestPermissionsApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder) {
        super(userManagementService, roleHolder);
    }

    @ApiOperation(value = "Tests read access to the specified project.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read items in the project."),
                   @ApiResponse(code = 403, message = "The current user cannot read items in the project."),
                   @ApiResponse(code = 403, message = "The project does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/read", restrictTo = Read)
    public ResponseEntity<String> testReadProjectAccess(final @PathVariable @Project String project) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        try{
            if(Permissions.canReadProject(getSessionUser(),project)){
                return new ResponseEntity<>(project, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests edit access to the specified project.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit items in the project."),
                   @ApiResponse(code = 403, message = "The current user cannot edit items in the project."),
                   @ApiResponse(code = 403, message = "The project does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/edit", restrictTo = Edit)
    public ResponseEntity<String> testEditProjectAccess(final @PathVariable @Project String project) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        try{
            if(Permissions.canEditProject(getSessionUser(),project)){
                return new ResponseEntity<>(project, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests delete access to the specified project.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete items in the project."),
                   @ApiResponse(code = 403, message = "The current user cannot delete items in the project."),
                   @ApiResponse(code = 403, message = "The project does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/delete", restrictTo = Delete)
    public ResponseEntity<String> testDeleteProjectAccess(final @PathVariable @Project String project) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        try{
            if(Permissions.canDeleteProject(getSessionUser(),project)){
                return new ResponseEntity<>(project, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests read access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot read the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/read", restrictTo = Read)
    public ResponseEntity<String> testReadSubjectAccess(final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        XnatSubjectdata sub = XnatSubjectdata.getXnatSubjectdatasById(subject, getSessionUser(), false);
        try{
            if(Permissions.canRead(getSessionUser(),sub)){
                return new ResponseEntity<>(subject, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests edit access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/edit", restrictTo = Edit)
    public ResponseEntity<String> testEditSubjectAccess(final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        XnatSubjectdata sub = XnatSubjectdata.getXnatSubjectdatasById(subject, getSessionUser(), false);
        try{
            if(Permissions.canEdit(getSessionUser(),sub)){
                return new ResponseEntity<>(subject, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests delete access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/delete", restrictTo = Delete)
    public ResponseEntity<String> testDeleteSubjectAccess(final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        XnatSubjectdata sub = XnatSubjectdata.getXnatSubjectdatasById(subject, getSessionUser(), false);
        try{
            if(Permissions.canDelete(getSessionUser(),sub)){
                return new ResponseEntity<>(subject, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests read access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot read the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "experiments/{experiment}/read", restrictTo = Read)
    public ResponseEntity<String> testReadExperimentAccess(final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        XnatExperimentdata exp = XnatExperimentdata.getXnatExperimentdatasById(experiment, getSessionUser(), false);
        try{
            if(Permissions.canRead(getSessionUser(),exp)){
                return new ResponseEntity<>(experiment, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests edit access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "experiments/{experiment}/edit", restrictTo = Edit)
    public ResponseEntity<String> testEditExperimentAccess(final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        XnatExperimentdata exp = XnatExperimentdata.getXnatExperimentdatasById(experiment, getSessionUser(), false);
        try{
            if(Permissions.canEdit(getSessionUser(),exp)){
                return new ResponseEntity<>(experiment, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Tests delete access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "experiments/{experiment}/delete", restrictTo = Delete)
    public ResponseEntity<String> testDeleteExperimentAccess(final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        XnatExperimentdata exp = XnatExperimentdata.getXnatExperimentdatasById(experiment, getSessionUser(), false);
        try{
            if(Permissions.canDelete(getSessionUser(),exp)){
                return new ResponseEntity<>(experiment, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        }
        catch(Exception e){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}
