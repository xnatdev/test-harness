/*
 * web: org.nrg.xapi.rest.data.CatalogApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.test.harness.xapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotAuthenticatedException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.*;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.nrg.xdat.security.helpers.AccessLevel.*;

@Api("XNAT Archive and Resource Management API")
@SuppressWarnings("RedundantThrows")
@XapiRestController
@RequestMapping(value = "/test/restrictTo")
@Slf4j
public class TestRestrictToApi extends AbstractXapiRestController {
    @Autowired
    public TestRestrictToApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder) {
        super(userManagementService, roleHolder);
    }

    @ApiOperation(value = "Tests read access to the specified project.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read items in the project."),
                   @ApiResponse(code = 403, message = "The current user cannot read items in the project."),
                   @ApiResponse(code = 403, message = "The project does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/read", restrictTo = Read)
    public String testReadProjectAccess(final @PathVariable @Project String project) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return project;
    }

    @ApiOperation(value = "Tests edit access to the specified project.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit items in the project."),
                   @ApiResponse(code = 403, message = "The current user cannot edit items in the project."),
                   @ApiResponse(code = 403, message = "The project does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/edit", restrictTo = Edit)
    public String testEditProjectAccess(final @PathVariable @Project String project) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return project;
    }

    @ApiOperation(value = "Tests delete access to the specified project.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete items in the project."),
                   @ApiResponse(code = 403, message = "The current user cannot delete items in the project."),
                   @ApiResponse(code = 403, message = "The project does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/delete", restrictTo = Delete)
    public String testDeleteProjectAccess(final @PathVariable @Project String project) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return project;
    }

    @ApiOperation(value = "Tests read access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot read the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/read", restrictTo = Read)
    public String testReadSubjectAccess(final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return subject;
    }

    @ApiOperation(value = "Tests edit access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/edit", restrictTo = Edit)
    public String testEditSubjectAccess(final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return subject;
    }

    @ApiOperation(value = "Tests delete access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/delete", restrictTo = Delete)
    public String testDeleteSubjectAccess(final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return subject;
    }

    @ApiOperation(value = "Tests read access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot read the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "experiments/{experiment}/read", restrictTo = Read)
    public String testReadExperimentAccess(final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return experiment;
    }

    @ApiOperation(value = "Tests edit access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "experiments/{experiment}/edit", restrictTo = Edit)
    public String testEditExperimentAccess(final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return experiment;
    }

    @ApiOperation(value = "Tests delete access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "experiments/{experiment}/delete", restrictTo = Delete)
    public String testDeleteExperimentAccess(final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return experiment;
    }

    @ApiOperation(value = "Tests read access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot read the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/subjects/{subject}/read", restrictTo = Read)
    public String testReadProjectSubjectAccess(final @PathVariable @Project String project, final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, subject);
    }

    @ApiOperation(value = "Tests edit access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/subjects/{subject}/edit", restrictTo = Edit)
    public String testEditProjectSubjectAccess(final @PathVariable @Project String project, final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, subject);
    }

    @ApiOperation(value = "Tests delete access to the specified subject.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the subject."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the subject."),
                   @ApiResponse(code = 403, message = "The subject does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/subjects/{subject}/delete", restrictTo = Delete)
    public String testDeleteProjectSubjectAccess(final @PathVariable @Project String project, final @PathVariable @Subject String subject) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, subject);
    }

    @ApiOperation(value = "Tests read access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot read the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/experiments/{experiment}/read", restrictTo = Read)
    public String testReadProjectExperimentAccess(final @PathVariable @Project String project, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, experiment);
    }

    @ApiOperation(value = "Tests edit access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/experiments/{experiment}/edit", restrictTo = Edit)
    public String testEditProjectExperimentAccess(final @PathVariable @Project String project, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, experiment);
    }

    @ApiOperation(value = "Tests delete access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/experiments/{experiment}/delete", restrictTo = Delete)
    public String testDeleteProjectExperimentAccess(final @PathVariable @Project String project, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, experiment);
    }

    @ApiOperation(value = "Tests read access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot read the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/subjects/{subject}/experiments/{experiment}/read", restrictTo = Read)
    public String testReadProjectSubjectExperimentAccess(final @PathVariable @Project String project, final @PathVariable @Subject String subject, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, subject, experiment);
    }

    @ApiOperation(value = "Tests edit access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/subjects/{subject}/experiments/{experiment}/edit", restrictTo = Edit)
    public String testEditProjectSubjectExperimentAccess(final @PathVariable @Project String project, final @PathVariable @Subject String subject, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, subject, experiment);
    }

    @ApiOperation(value = "Tests delete access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "projects/{project}/subjects/{subject}/experiments/{experiment}/delete", restrictTo = Delete)
    public String testDeleteProjectSubjectExperimentAccess(final @PathVariable @Project String project, final @PathVariable @Subject String subject, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", project, subject, experiment);
    }

    @ApiOperation(value = "Tests read access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can read the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot read the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/experiments/{experiment}/read", restrictTo = Read)
    public String testReadProjectSubjectExperimentAccess(final @PathVariable @Subject String subject, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", subject, experiment);
    }

    @ApiOperation(value = "Tests edit access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can edit the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot edit the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/experiments/{experiment}/edit", restrictTo = Edit)
    public String testEditProjectSubjectExperimentAccess(final @PathVariable @Subject String subject, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", subject, experiment);
    }

    @ApiOperation(value = "Tests delete access to the specified experiment.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user can delete the experiment."),
                   @ApiResponse(code = 403, message = "The current user cannot delete the experiment."),
                   @ApiResponse(code = 403, message = "The experiment does not exist."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = "subjects/{subject}/experiments/{experiment}/delete", restrictTo = Delete)
    public String testDeleteProjectSubjectExperimentAccess(final @PathVariable @Subject String subject, final @PathVariable @Experiment String experiment) throws InsufficientPrivilegesException, NotAuthenticatedException, NotFoundException {
        return StringUtils.joinWith(":", subject, experiment);
    }

    @ApiOperation(value = "Tests if the authorizing user belongs to the ALL_DATA_ACCESS group.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user has correct access."),
                   @ApiResponse(code = 403, message = "The current user does not have the correct access.")})
    @XapiRequestMapping(value = "dataAccess", restrictTo = DataAccess)
    public String testAllDataAccess() throws InsufficientPrivilegesException, NotAuthenticatedException {
        return "valid";
    }

    @ApiOperation(value = "Tests if the authorizing user belongs to the ALL_DATA_ADMIN group.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The current user has correct access."),
                   @ApiResponse(code = 403, message = "The current user does not have the correct access.")})
    @XapiRequestMapping(value = "dataAdmin", restrictTo = DataAdmin)
    public String testAllDataAdmin() throws InsufficientPrivilegesException, NotAuthenticatedException {
        return "valid";
    }

}
