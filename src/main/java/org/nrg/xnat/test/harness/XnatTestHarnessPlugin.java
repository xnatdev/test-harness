package org.nrg.xnat.test.harness;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "xnat-test-harness-plugin", name = "XNAT Test Harness Plugin", logConfigurationFile = "xnat-test-harness-logback.xml")
@ComponentScan("org.nrg.xnat.test.harness.xapi")
@Slf4j
public class XnatTestHarnessPlugin {
}
