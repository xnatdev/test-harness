# XNAT Test Harness Plugin #

This is the XNAT Test Harness Plugin. It provides various APIs, screens, and actions for testing core XNAT functions.

## Building ##

To build the XNAT test harness plugin:

1. If you haven't already, clone [this repository](https://bitbucket.org/xnatdev/test-harness-plugin.git) and cd to the newly cloned folder.

1. Build the plugin:

    ```bash
    ./gradlew clean jar 
    ```
    
    On Windows, you can use the batch file:
    
    ```bash
    gradlew.bat clean jar
    ```
    
    This should build the plugin in the file **build/libs/xnat-test-harness-plugin-_version_.jar**.
    
1. Copy the plugin jar to your plugins folder: 

    ```bash
    cp build/libs/xnat-test-harness-plugin-_version_.jar /data/xnat/home/plugins
    ```
